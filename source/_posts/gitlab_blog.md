---
title: Gitlab Pages 搭建 Hexo 博客
data: 2017-10-12 09:05:28
author: Seek Cloud
blog: /
issueId: gitlab_blog
github: https://github.com/Seek-cloud
categories: [教程]
tags: [Gitlab Pages, Blog, Hexo]
---

> 请先安装Git和NodeJs

## **一、申请[Gitlab](https://gitlab.com/users/sign_in)账户，创建Pages的项目**

1. 可以使用GitHub账户授权登录
1. 获取当前用户的username(可以说是你的项目仓库路径的标识，不是你的名字)
 ![](/images/gitlab_blog/0.png)
<!-- more -->
<The rest of contents | 余下全文>
1. 创建一个新的项目命名为username.gitlab.io(项目名小写，注意即使你的username有大写的，项目名也得小写)，
比如我的username是`Seek-cloud`，我的项目名就得为`seek-cloud.gitlab.io`
 ![](/images/gitlab_blog/1.png)

## **二、获取Hexo博客**

1. 安装hexo-cli命令行工具
```bash
npm i hexo-cli -g
```
1. 创建一个新目录，然后再这个目录下初始化hexo(后续操作的根目录都是这个目录)
```bash
hexo i
```
 ![](/images/gitlab_blog/2.png)
 执行完毕后你会看到如下一些文件
 ![](/images/gitlab_blog/3.png)
1. 启动hexo博客服务
```bash
hexo s
```
 ![](/images/gitlab_blog/4.png)
1. 浏览器访问[http://localhost:4000](http://localhost:4000)
 ![](/images/gitlab_blog/5.png)

## **三、上传博客至Gitlab创建的项目**
1. 在当前根目录下创建一个 `.gitlab-ci.yml` Gitlab命令行配置文件，输入以下内容
```bash
# 使用Node 4.2.2
image: node:4.2.2

# 定义一个生成页面的Job
pages:
  cache:
    # 这些目录将缓存起来，供多个Job使用
    paths:
    - node_modules/

  # 这个Job要执行的命令
  script:
  - npm i hexo-cli -g
  - npm i
  - hexo d
  artifacts:
    # 这些文件夹下的文件将对外开发
    paths:
    - public
  # 指定的这些版本才会执行
  only:
  - master
```
1. 打开Git bash，生成你Gitlab账户的一个ssh key，更换为自己的用户名和邮箱
```bash
git config --global user.name "username"
git config --global user.email "email"
ssh-keygen -t rsa -C "email"
```
 ![](/images/gitlab_blog/6.png)
1. 在你Gitlab个人设置里的SSH key的key里面填入你生成*.pub的所有内容，添加key
 ![](/images/gitlab_blog/7.png)
1. 在当前根目录执行(请更换为你项目的git地址)
```bash
git init
git remote add origin git@gitlab.com:username/username.gitlab.io.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
1. 上传完毕后，gitlab的会自动执行gitlab-ci.yml配置的Job
 ![](/images/gitlab_blog/8.png)
1. 执行完毕后，没有问题的话就可以访问了https://username.gitlab.io
