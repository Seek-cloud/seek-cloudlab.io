---
title: JS 数组操作
author: Seek Cloud
blog: /
issueId: js-array
github: https://github.com/Seek-cloud
categories: [JavaScript]
tags: [Array]
---

## **一、Windows安装**

1. *`unshift`*： 将参数添加到原数组开头，并返回数组的长度
```js
var a = [1,2,3,4,5];
var b = a.unshift(-2,-1);
// a：[-2,-1,1,2,3,4,5]   b：7
```
1. *`push`*： 将参数添加到原数组末尾，并返回数组的长度
```js
var a = [1,2,3,4,5];
var b = a.push(6,7);
// a：[1,2,3,4,5,6,7]   b：7
```

<!-- more -->
<The rest of contents | 余下全文>

## **二、删除**  

1. *`pop`*： 删除原数组最后一项，并返回删除元素的值；如果数组为空则返回undefined
```js
var a = [1,2,3,4,5];
var b = a.pop();
// a：[1,2,3,4]   b：5
```
1. *`shift`*：删除原数组第一项，并返回删除元素的值；如果数组为空则返回undefined
```js
var a = [1,2,3,4,5];
var b = a.shift();
//a：[2,3,4,5]   b：1
```

## **三、其他**
1. *`concat()`*： 返回一个新数组，是将参数添加到原数组中构成的
```js
var a = [1,2,3,4,5];
var b = a.concat(6,7);
//a：[1,2,3,4,5]   b：[1,2,3,4,5,6,7]
```
1. *`splice(start,deleteCount,val1,val2,...)`*：从start位置开始删除deleteCount项，并从该位置起插入val1,val2,...
```js
var a = [1,2,3,4,5];
var b = a.splice(2,2,7,8,9);
//a：[1,2,7,8,9,5]   b：[3,4]
```
1. *`reverse()`*：将数组反序
```js
var a = [1,2,3,4,5];
var b = a.reverse();
//a：[5,4,3,2,1]   b：[5,4,3,2,1]
```
1. *`sort(orderfunction)`*：按指定的参数对数组进行排序
```js
var a = [1,2,3,4,5];
var b = a.sort();
//a：[1,2,3,4,5]   b：[1,2,3,4,5]
```
1. *`slice(start,end)`*：返回从原数组中指定开始下标到结束下标之间的项组成的新数组
```js
var a = [1,2,3,4,5];
var b = a.slice(2,5);
//a：[1,2,3,4,5]   b：[3,4,5]
```
1. *`join(separator)`*：将数组的元素组起一个字符串，以separator为分隔符，省略的话则用默认用逗号为分隔符
```js
var a = [1,2,3,4,5];
var b = a.join("|");
//a：[1,2,3,4,5]   b："1|2|3|4|5"
```