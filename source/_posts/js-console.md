---
title: JS Console
author: Seek Cloud
blog: /
issueId: js-console
github: https://github.com/Seek-cloud
categories: [JavaScript]
tags: [console]
---

## **一、 信息输出**
1. *`log,info,error,warn`*： 显示不同类型的信息
```js
console.log('hello');
console.info('信息');
console.error('错误');
console.warn('警告');
```
1. 可以使用占位符
```js
console.log("%d年%d月%d日",2011,3,26);
```
<!-- more -->
<The rest of contents | 余下全文>

## **二、信息分组**
1. *`group,groupEnd`*： 可折叠的组信息
```js
console.group("第一组信息");
    console.log("第一组第一条:");
    console.log("第一组第二条:");
console.groupEnd();
console.group("第二组信息");
    console.log("第二组第一条");
    console.log("第二组第二条");
console.groupEnd();
```

## **三、查看对象信息**
1. *`dir()`*： 显示一个对象所有的属性和方法
```js
var info = {
    A:"AAA",
    B:"BBB",
    C:"CCC"
};
console.dir(info);
```

## **四、显示某个节点的内容**
1. *`dirxml()`*： 用来显示网页的某个节点（node）所包含的html/xml代码
```js
var info = document.getElementById('XXX');
console.dirxml(info);
```

## **五、判断变量是否是真**

1. *`assert()`*： 用来判断一个表达式或变量是否为真。如果结果为否，则在控制台输出一条相应信息，并且抛出一个异常
```js
var result = 1;
console.assert(result);
var year = 2014;
console.assert(year == 2018 );
```

## **六、追踪函数的调用轨迹**

1. *`trace()`*： 追踪函数的调用轨迹
```js
function add(a, b){
    console.trace();
    return a + b;
}
var x = add3(1,1);
function add3(a, b)	{return add2(a, b);}
function add2(a, b)	{return add1(a, b);}
function add1(a, b)	{return add(a, b);}
```

## **七、计时功能**
1. *`time(),timeEnd()`*： 用来显示代码的运行时间
```js
console.time("控制台计时器一");
for(var i = 0; i < 1000; i++) {
    for(var j = 0; j < 1000; j++) { }
}
console.timeEnd("控制台计时器一");
```

## **八、性能分析**
1. *`profile()`*： 性能分析
```js
function All() {
    alert(11);
    for(var i = 0;i < 10; i++) {
        funcA(1000);
    }
    funcB(10000);
}

function funcA(count) {
    for(var i = 0; i < count; i++) { }
}

function funcB(count) {
    for(var i = 0; i < count; i++) { }
}

console.profile('性能分析器');
All();
console.profileEnd();
```


