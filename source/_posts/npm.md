---
title: npm安装
date: 2017-10-10 09:51:44
author: Seek Cloud
blog: /
issueId: npm
github: https://github.com/Seek-cloud
categories: [基础知识]
tags: [NodeJS, npm]
---

## **一、Windows安装**

1. 下载[NodeJS](https://nodejs.org/en/download/)压缩包
1. 设置Node的安装目录到path环境变量
1. 修改全局路径-路径随意(并把这个地址添加到path环境变量)
```bash
npm config set prefix "E:\NodeJS\node_modules\node_global"
```
1. 修改缓存路径
```bash
npm config set cache "E:\NodeJS\node_modules\node_cache"
```

## **二、Linux安装**
&emsp;&emsp;&emsp;&emsp;**`TODO`**
